<?php

namespace Kaskadia\Lib\Routing;

use Illuminate\Routing\Route as LaravelRoute;
use Illuminate\Routing\Router;

class RouteManager {
    /**
     * @param Router $router
     * @param RouteGroup[] $routeGroups
     */
    public static function createRoutesFromGroups(Router $router, array $routeGroups): void {
        foreach($routeGroups as $group) {
            $router->group($group->getGroupAttributes(), function(Router $router) use ($group) {
                if(!empty($group->getGroups())) {
                    self::createRoutesFromGroups($router, $group->getGroups());
                }
                self::createGetRoutes($router, $group->getGetRoutes());
                self::createPostRoutes($router, $group->getPostRoutes());
            });
        }
    }

    /**
     * @param Router $router
     * @param Route[] $routes
     */
    public static function createGetRoutes(Router $router, array $routes): void {
        foreach($routes as $route) {
            $r = $router->get($route->getUri(), $route->getAction());
            self::decorateRoute($route, $r);
        }
    }

    /**
     * @param Router $router
     * @param Route[] $routes
     */
    public static function createPostRoutes(Router $router, array $routes): void {
        foreach($routes as $route) {
            $r = $router->post($route->getUri(), $route->getAction());
            self::decorateRoute($route, $r);
        }
    }

    private static function decorateRoute(Route $route, LaravelRoute $r) {
        if($route->getName() !== null) {
            $r->name($route->getName());
        }
        if(!empty($route->getMiddleware())) {
            $r->middleware($route->getMiddleware());
        }
    }
}
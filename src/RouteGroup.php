<?php

namespace Kaskadia\Lib\Routing;

class RouteGroup {
    //<editor-fold defaultstate="collapsed" name="CONSTANTS">
    private const PREFIX = "prefix";
    private const MIDDLEWARE = "middleware";
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" name="PROPERTIES">
    /** @var RouteGroup[] */
    private array $groups;
    /** @var Route[] */
    private array $getRoutes;
    /** @var Route[] */
    private array $postRoutes;
    private ?string $prefix;
    /** @var string[] */
    private array $middleware;
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" name="INIT">
    /**
     * RouteGroup constructor.
     * @param array $groups
     * @param array $getRoutes
     * @param array $postRoutes
     * @param string|null $prefix
     * @param array $middleware
     */
    private function __construct(array $groups = [], array $getRoutes = [], array $postRoutes = [], ?string $prefix = null, array $middleware = []) {
        $this->groups = $groups;
        $this->getRoutes = $getRoutes;
        $this->postRoutes = $postRoutes;
        $this->prefix = $prefix;
        $this->middleware = $middleware;
    }

    /**
     * @return RouteGroup
     */
    public static function initializeNew(): self {
        return new self();
    }

    /**
     * @param Route[] $getRoutes
     * @param Route[] $postRoutes
     * @param string|null $prefix
     * @param string[] $middleware
     * @return RouteGroup
     */
    public static function initializeWithBothRoutes(array $getRoutes, array $postRoutes, ?string $prefix = null, array $middleware = []): self {
        return new self([], $getRoutes, $postRoutes, $prefix, $middleware);
    }

    /**
     * @param Route[] $getRoutes
     * @param string|null $prefix
     * @param string[] $middleware
     * @return RouteGroup
     */
    public static function initializeWithGetRoutes(array $getRoutes, ?string $prefix = null, array $middleware = []):self {
        return new self([], $getRoutes, [], $prefix, $middleware);
    }

    /**
     * @param Route[] $postRoutes
     * @param string|null $prefix
     * @param string[] $middleware
     * @return RouteGroup
     */
    public static function initializeWithPostRoutes(array $postRoutes, ?string $prefix = null, array $middleware = []): self {
        return new self([], [], $postRoutes, $prefix, $middleware);
    }

    /**
     * @param RouteGroup[] $groups
     * @param Route[] $getRoutes
     * @param Route[] $postRoutes
     * @param string|null $prefix
     * @param string[] $middleware
     * @return RouteGroup
     */
    public static function initializeWithGroups(array $groups, array $getRoutes = [], array $postRoutes = [], ?string $prefix = null, array $middleware = []): self {
        return new self($groups, $getRoutes, $postRoutes, $prefix, $middleware);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" name="ACCESSORS">
    /**
     * @return RouteGroup[]
     */
    public function getGroups(): array {
        return $this->groups;
    }

    /**
     * @param RouteGroup $group
     * @return $this
     */
    public function addGroup(RouteGroup $group): self {
        $this->groups[] = $group;
        return $this;
    }

    /**
     * @return Route[]
     */
    public function getGetRoutes(): array {
        return $this->getRoutes;
    }

    /**
     * @param Route $route
     * @return $this
     */
    public function addGetRoute(Route $route): self {
        $this->getRoutes[] = $route;
        return $this;
    }

    /**
     * @return Route[]
     */
    public function getPostRoutes(): array {
        return $this->postRoutes;
    }

    /**
     * @param Route $route
     * @return $this
     */
    public function addPostRoute(Route $route): self {
        $this->postRoutes[] = $route;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPrefix(): ?string {
        return $this->prefix;
    }

    public function setPrefix(string $prefix): self {
        $this->prefix = $prefix;
        return $this;
    }

    /**
     * @return string[]
     */
    public function getMiddleware(): array {
        return $this->middleware;
    }

    public function addMiddleware(string $middleware): self {
        $this->middleware[] = $middleware;
        return $this;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" name="METHODS">
    public function getGroupAttributes(): array {
        $attributes = [];
        if(isset($this->prefix)) {
            $attributes[self::PREFIX] = $this->getPrefix();
        }
        if(!empty($this->middleware)) {
            $attributes[self::MIDDLEWARE] = $this->getMiddleware();
        }
        return $attributes;
    }
    //</editor-fold>
}
<?php

namespace Kaskadia\Lib\Routing;

class Route {
    //<editor-fold defaultstate="collapsed" name="PROPERTIES">
    private string $uri;
    private string $action;
    private ?string $name;
    /** @var string[] $middleware */
    private array $middleware;
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" name="INIT">
    private function __construct(string $uri, string $action, ?string $name, array $middleware = []) {
        $this->uri = $uri;
        $this->action = $action;
        $this->name = $name;
        $this->middleware = $middleware;
    }

    public static function initialize(string $uri, string $action, ?string $name = null): self {
        return new self($uri, $action, $name);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" name="ACCESSORS">
    public function getUri(): string {
        return $this->uri;
    }

    public function getAction(): string {
        return $this->action;
    }

    public function getName(): ?string {
        return $this->name;
    }

    /**
     * @return string[]
     */
    public function getMiddleware(): array {
        return $this->middleware;
    }
    //</editor-fold>
}